FROM mhart/alpine-node:12.16.1 as build 
WORKDIR /app

COPY src/ src/
COPY *.json ./
COPY webpack.prod.js ./

RUN npm install yarn -g
RUN yarn install
RUN yarn run build:prod

FROM mhart/alpine-node:12.16.1 as prod

ENV PORT=3000
# ENV SQL_STRING=
ENV JOB_ENABLED=true
ENV API_ENABLED=true

WORKDIR /app

COPY --from=build app/prod/ ./

EXPOSE 3000/tcp
ENTRYPOINT [ "node", "server.js" ]