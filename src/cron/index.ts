import { CronJob } from 'cron';
import {
  insertData,
  getLatestUpdateDate,
  checkIfTableExists,
  createTableForCountry,
  createDbConnection
} from '../sql';
import { getData, STARTING_DATE } from '../dataSources/covid19api';
import { ConnectionPool } from 'mssql';

import COUNTRY_DATA from '../countries.json';

const DAYS_STEP = Number(process.env.SYNCHRONIZATION_PERIOD || 7);

export const jobStatus = {
  isRunning: false
};

export const job = new CronJob(
  '0 0 23 * * *' /* 23:00 everyday */,
  async () => {
    if (!jobStatus.isRunning) {
      console.log('Cron job invoked.', new Date());
      jobStatus.isRunning = true;
      await synchronizeAllData();
      jobStatus.isRunning = false;
    } else {
      console.log(
        'Cron job skipped, synchronization is already running.',
        new Date()
      );
    }
  },
  null,
  false
);

const getMaxSyncDate = () => {
  const today = new Date();
  today.setDate(today.getDate() - 1);
  return today;
};

function getNextDay(date: Date) {
  var result = new Date(date.getFullYear(), date.getMonth(), date.getDate());
  result.setDate(result.getDate() + 1);
  return result;
}

function getNextStepDate(date: Date) {
  const maxDate = getMaxSyncDate();
  var result = new Date(date.getFullYear(), date.getMonth(), date.getDate());
  result.setDate(result.getDate() + DAYS_STEP);

  return result > maxDate ? maxDate : result;
}

async function updateDataFromDate(
  connection: ConnectionPool,
  startingDate: Date,
  country: string
) {
  const today = new Date();

  // check for last date in DB
  let sDate = startingDate;
  let eDate = getNextStepDate(sDate);
  while (!dateEquals(today, sDate) && sDate < today) {
    console.log(
      'Updating data between',
      sDate.toLocaleDateString(),
      eDate.toLocaleDateString()
    );
    const [success, data] = await getData(sDate, eDate, country);
    if (success) {
      await insertData(connection, data, country);
      sDate = getNextDay(eDate);
      eDate = getNextStepDate(sDate);
    } else {
      throw `Could not fetch data for ${country} on ${sDate}`;
    }
  }
}

export async function synchronizeAllData() {
  const connection = await createDbConnection();
  const updatedCountries = [];
  const failedCountries = [];

  for (const k in COUNTRY_DATA) {
    const country = (COUNTRY_DATA as any)[k];
    console.log('Synchronizing for:', country, new Date());
    console.log('Starting data synchronization for:', country);

    try {
      await synchronize(connection, country);
      updatedCountries.push(country);
    } catch (err) {
      console.error(err);
      failedCountries.push(country);
    }
  }
  console.log('SUCCESFUL MIGRATIONS:', updatedCountries);
  console.log('FAILED MIGRATIONS:', failedCountries);
}

const synchronize = async (
  connection: ConnectionPool,
  country: string
): Promise<void> => {
  if (!(await checkIfTableExists(connection, country))) {
    await createTableForCountry(connection, country);
  }
  let [success, startingDate] = await getLatestUpdateDate(connection, country);

  if (success) {
    startingDate = getNextDay(startingDate);
  } else {
    startingDate = STARTING_DATE;
  }
  await updateDataFromDate(connection, startingDate, country);
};

function dateEquals(dateA: Date, dateB: Date) {
  return (
    dateA.getFullYear() === dateB.getFullYear() &&
    dateA.getMonth() === dateB.getMonth() &&
    dateA.getDate() == dateB.getDate()
  );
}
