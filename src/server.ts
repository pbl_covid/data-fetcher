// import './aliases';
import dotenv from 'dotenv';
dotenv.config();
import { job, jobStatus, synchronizeAllData } from './cron';
import express from 'express';
import { createCsv } from './csv';
import { getDataAsync, getAvailableCountries } from './sql';
import cors from 'cors';

const jobEnabled = (process.env.JOB_ENABLED || '').toLowerCase() == 'true';
const apiEnabled = (process.env.API_ENABLED || '').toLowerCase() == 'true';

const settings: {
  jobEnabled: boolean;
  apiEnabled: boolean;
  lastJobRun?: Date;
} = {
  jobEnabled,
  apiEnabled,
  lastJobRun: undefined
};
const normalizePort = (port: any) => parseInt(port, 10);
const port = normalizePort(process.env.PORT || 3000);

const app = express();
app.use(cors());

if (jobEnabled) {
  app.post('/force', async (req, res) => {
    if (!jobStatus.isRunning) {
      settings.lastJobRun = new Date();
      jobStatus.isRunning = true;
      res.json('Force synchronization started...');
      synchronizeAllData().then(() => (jobStatus.isRunning = false));
    } else {
      res.json('Force synchronization is already running...');
    }
  });

  job.start();
}

if (apiEnabled) {
  app.get('/countries', async (req, res) => {
    const countries = await getAvailableCountries();
    res.json(countries);
  });

  app.get('/csv', async (req, res) => {
    const { country, start, end } = req.query;
    const data: CovidDataDbModel[] = await getDataAsync(
      country as string,
      start as string,
      end as string
    );
    const csv = createCsv<CovidDataDbModel>(data);
    res.header('Content-Type', 'text/csv');
    res.send(csv);
  });
}

app.get('/healthz', async (req, res) => {
  const result = { ...settings };
  result.lastJobRun =
    job.lastDate() > settings.lastJobRun!
      ? job.lastDate()
      : settings.lastJobRun;
  console.log(result);
  res.json(result);
});

app.listen(
  port,
  () => `Covid service started on port ${port} with settings: ${settings}`
);
