interface CovidDataRow {
  provinceState: string;
  // countryRegion: string;
  city: string;
  cityCode: string;
  lastUpdate: string;
  confirmed: string;
  deaths: string;
  recovered: string;
  active: string;
  // incidentRate: String;
  // fatalityRatio: String;
}

interface CovidDataDbModel {
  Province: string;
  City: string;
  CityCode: string;
  LastUpdate: Date;
  Confirmed: number;
  Active: number;
  Deaths: number;
  Recovered: number;
}

interface AvailableCountryDto {
  Name: string;
  Code: string;
}
