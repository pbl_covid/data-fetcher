export function sanitize(str: string) {
  return str
    .normalize('NFD')
    .replace(/[\u0300-\u036f]/g, '')
    .replace(/'/g, '');
}
export function sanitizeRow(row: CovidDataRow) {
  row.provinceState = sanitize(row.provinceState);
  row.city = sanitize(row.city);
  row.cityCode = sanitize(row.cityCode);

  if (!isInt(row.confirmed)) {
    row.confirmed = 'NULL';
  }
  if (!isInt(row.deaths)) {
    row.deaths = 'NULL';
  }
  if (!isInt(row.recovered)) {
    row.recovered = 'NULL';
  }
  if (!isInt(row.active)) {
    row.active = 'NULL';
  }
}

function isInt(n: String) {
  const number = Number(n);
  if (isNaN(number)) {
    return false;
  }

  return n !== '' && number % 1 === 0;
}

function isFloat(n: String) {
  const number = Number(n);
  if (isNaN(number)) {
    return false;
  }

  return n !== '' && number % 1 !== 0;
}
