import { ConnectionPool } from 'mssql';
import { sanitizeRow, sanitize } from './utility';
import dateFormat from 'dateformat';

const AVAILABLE_COUNTRIES_TABLE = 'countries';
const CREATE_AVAILABLE_COUNTRIES_TABLE_QUERY =
  `CREATE TABLE ${AVAILABLE_COUNTRIES_TABLE}` +
  ' (Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY, Name varchar(255), Code varchar(255))';
const GET_AVAILABLE_COUNTRIES_QUERY = `SELECT * FROM ${AVAILABLE_COUNTRIES_TABLE}`;

function addAvailableCountryQuery(
  countryDisplayName: string,
  countryCode: string
): string {
  return `INSERT INTO ${AVAILABLE_COUNTRIES_TABLE} (Name, Code) VALUES ('${countryDisplayName}', '${sanitizeSqlTableName(
    countryCode
  )}')`;
}

const sqlColumns =
  'Province, City, CityCode, Confirmed, Deaths, Recovered, Active, LastUpdate';

async function addAvailableCountry(
  connection: ConnectionPool,
  countryDisplayName: string,
  countryCode: string
) {
  if (!(await checkIfTableExists(connection, AVAILABLE_COUNTRIES_TABLE))) {
    await connection.query(CREATE_AVAILABLE_COUNTRIES_TABLE_QUERY);
  }
  await connection.query(
    addAvailableCountryQuery(countryDisplayName, countryCode)
  );
}

export async function getAvailableCountries(): Promise<AvailableCountryDto[]> {
  const connection = await createDbConnection();
  const { recordset } = await connection.query<AvailableCountryDto>(
    GET_AVAILABLE_COUNTRIES_QUERY
  );
  const result = recordset.map((r) => r);
  await connection.close();

  return result;
}

export async function createDbConnection(): Promise<ConnectionPool> {
  let connPool: ConnectionPool;
  const connectionString = process.env.SQL_STRING;
  if (connectionString) {
    connPool = new ConnectionPool(connectionString);
  } else {
    connPool = new ConnectionPool({
      database: process.env.SQL_DATABASE!,
      user: process.env.SQL_USER!,
      password: process.env.SQL_PASSWORD!,
      port: Number(process.env.SQL_PORT),
      server: process.env.SQL_SERVER!
    });
  }

  if (!connPool.connected) {
    try {
      const connected = await connPool.connect();
    } catch (error) {
      console.error(error);
      throw error;
    }
  }
  return connPool;
}

export async function insertData(
  connection: ConnectionPool,
  data: CovidDataRow[],
  tableName: string
) {
  // TODO: run migration SQL
  const batchSize = 100;
  // const transaction = await connPool.transaction().begin();
  try {
    while (data.length > 0) {
      const batched = data.splice(0, batchSize);

      batched.forEach((row) => sanitizeRow(row));

      const sqlData = batched
        .map(
          (d) =>
            `('${d.provinceState}','${d.city}','${d.cityCode}',${d.confirmed},${d.deaths},${d.recovered},${d.active},'${d.lastUpdate}')`
        )
        .join(',');
      const query = `INSERT INTO ${sanitizeSqlTableName(
        tableName
      )} (${sqlColumns}) VALUES ${sqlData}`;
      await connection.query(query);
    }

    // transaction.commit();
  } catch (error) {
    // transaction.rollback();
    console.error('SQL Data insert error', error);
    throw error;
  }
}

export async function getLatestUpdateDate(
  connection: ConnectionPool,
  country: string
): Promise<[boolean, Date]> {
  const query = `SELECT TOP 1 LastUpdate FROM ${sanitizeSqlTableName(
    country
  )} ORDER BY LastUpdate DESC`;
  const res = await connection.query(query);
  if (res.recordset.length === 1) {
    return [true, new Date(res.recordset[0]['LastUpdate'])];
  }
  return [false, new Date()];
}

export async function createTableForCountry(
  connection: ConnectionPool,
  country: string
): Promise<boolean> {
  if (await checkIfTableExists(connection, country)) {
    return false;
  }
  await addAvailableCountry(connection, country, country);
  const createTableQuery =
    `CREATE TABLE ${sanitizeSqlTableName(country)}` +
    ' (Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY, Province varchar(255), City varchar(255), CityCode varchar(255), Confirmed INT, Deaths INT, Recovered INT, Active INT, LastUpdate Date)';
  const { rowsAffected } = await connection.query(createTableQuery);

  return rowsAffected.length === 1 && rowsAffected[0] === 1;
}

export async function checkIfTableExists(
  connection: ConnectionPool,
  country: string
): Promise<boolean> {
  const checkIfExistsQuery =
    `IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='${sanitizeSqlTableName(
      country
    )}')) ` + 'SELECT 1 AS res ELSE SELECT 0 AS res';
  const { recordset } = await connection.query(checkIfExistsQuery);

  return recordset.length === 1 && recordset[0]['res'] == 1;
}

const sanitizeSqlTableName = (name: string): string => {
  return sanitize(name).replace(/-/g, '');
};

async function _getDataAsync(
  connection: ConnectionPool,
  table: string,
  startingDate: string,
  endingDate: string
): Promise<CovidDataDbModel[]> {
  let query = `SELECT ${sqlColumns} FROM ${sanitizeSqlTableName(table)}`;

  if (startingDate != null && endingDate != null) {
    query += ` WHERE LastUpdate >= '${formatSqlDate(
      new Date(startingDate)
    )}' AND LastUpdate <= '${formatSqlDate(new Date(endingDate))}'`;
  }

  query += ' ORDER BY LastUpdate';

  const { recordset } = await connection.query<CovidDataDbModel>(query);
  const result = recordset.map((r) => r);

  return result;
}
export async function getDataAsync(
  table: string,
  startingDate: string,
  endingDate: string
): Promise<CovidDataDbModel[]> {
  const connection = await createDbConnection();
  const result = await _getDataAsync(
    connection,
    table,
    startingDate,
    endingDate
  );
  connection.close();
  return result;
}

const formatSqlDate = (date: Date) => dateFormat(date, 'yyyy-mm-dd');
