import { asyncRetry, sleep } from '../../utility';
import axios from 'axios';
import dateFormat from 'dateformat';
const apiUrl = 'https://api.covid19api.com';
const TIMEOUT = 30000;

export const STARTING_DATE = new Date('2020-01-22');

interface CovidAPIDto {
  Country: string;
  CountryCode: string;
  Province: string;
  City: string;
  CityCode: string;
  Lat: string;
  Lon: string;
  Confirmed: number;
  Deaths: number;
  Recovered: number;
  Active: number;
  Date: string;
}

interface CountryAPIDto {
  Country: string;
  Slug: string;
  ISO2: string;
}

const urlForDate = (sdate: Date, edate: Date, country: string): string => {
  const formattedStartingDate = dateFormat(sdate, 'yyyy-mm-dd');
  const formattedEndingDate = dateFormat(edate, 'yyyy-mm-dd');
  return `${apiUrl}/country/${country}?from=${formattedStartingDate}T00:00:00Z&to=${formattedEndingDate}T23:59:00Z`;
};

const dayOneUrl = (country: string) => `${apiUrl}/dayone/country/${country}`;

const getCountriesUrl = `${apiUrl}/countries`;

export const getAvailableCountries = async (): Promise<[boolean, string[]]> => {
  const { status, data } = await axios.get<CountryAPIDto[]>(getCountriesUrl);
  if (status == 200) {
    return [true, data.map((x) => x.Slug)];
  }
  return [false, []];
};

export const getDataDayOne = async (
  country: string
): Promise<[boolean, CovidDataRow[], boolean]> => {
  try {
    return [
      ...(await asyncRetry(async () => await _getDataDayOne(country))),
      false
    ];
  } catch (error) {
    return [false, [], true];
  }
};

const _getDataDayOne = async (
  country: string
): Promise<[boolean, CovidDataRow[]]> => {
  sleep(1); // prevent too many request error
  const url = dayOneUrl(country);
  const { status, data } = await axios.get<CovidAPIDto[]>(url, {
    timeout: TIMEOUT
  });

  if (status == 200) {
    return [
      true,
      data.map<CovidDataRow>((d) => ({
        provinceState: d.Province,
        city: d.City,
        cityCode: d.CityCode,
        confirmed: d.Confirmed.toString(),
        deaths: d.Deaths.toString(),
        recovered: d.Recovered.toString(),
        active: d.Active.toString(),
        lastUpdate: d.Date
      }))
    ];
  }
  return [false, []];
};

export const getData = async (
  sdate: Date,
  edate: Date,
  country: string
): Promise<[boolean, CovidDataRow[]]> =>
  asyncRetry(() => _getData(sdate, edate, country));

const _getData = async (
  sdate: Date,
  edate: Date,
  country: string
): Promise<[boolean, CovidDataRow[]]> => {
  sleep(1); // prevent too many request error
  const url = urlForDate(sdate, edate, country);
  const { status, data } = await axios.get<CovidAPIDto[]>(url, {
    timeout: TIMEOUT
  });
  if (status == 200) {
    return [
      true,
      data.map<CovidDataRow>((d) => ({
        confirmed: d.Confirmed.toString(),
        deaths: d.Deaths.toString(),
        recovered: d.Recovered.toString(),
        active: d.Active.toString(),
        countryRegion: d.Country,
        provinceState: d.Province,
        city: d.City,
        cityCode: d.CityCode,
        lastUpdate: d.Date
      }))
    ];
  }
  return [false, []];
};
