# Aliases

Add aliases to tsconfig.json path

# TODO

- Check last updated date
- Check current date
- Download files with current date until it fails
- Add downloaded data to db
- if any data is not valid send an email
- API ENDPOINTS:
  - force update
  - healthcheck
  - download data as csv
- _(\*create db backup)_

- data sources:
  - CSSE at Johns Hopkins University: https://github.com/CSSEGISandData/COVID-19/tree/master/csse_covid_19_data/csse_covid_19_daily_reports

# AZURE SQL

pbl-admin
polsl20201!
